package org.example.b1b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class B1bApplication {

    public static void main(String[] args) {
        System.out.println("Hello World");
        SpringApplication.run(B1bApplication.class, args);
        System.out.println("Finish Hello World");
    }
}
