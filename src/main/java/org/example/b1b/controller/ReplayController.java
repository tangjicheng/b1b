package org.example.b1b.controller;

import org.example.b1b.service.GlobalService;
import org.example.b1b.utils.FixUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import quickfix.Message;
import quickfix.Session;
import quickfix.SessionNotFound;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ReplayController {

    private final GlobalService globalService;

    public ReplayController(GlobalService globalService) {
        this.globalService = globalService;
    }

    @PostMapping("/replay")
    public String startReplay() throws SessionNotFound {
        var sess = globalService.getSessionID();
        List<Message> orders = FixUtils.parseOrdersFromFile(globalService.getReplayFilePath());
        System.out.printf("Orders size: %d\n", orders.size());
        System.out.printf("Replay file path: %s\n", globalService.getReplayFilePath());
        System.out.printf("SessionID: %s\n", sess.toString());
        for (Message order : orders) {
            Session.sendToTarget(order, sess);
        }
        return "Replay started successfully\n";
    }
}
