package org.example.b1b.service;

import java.time.LocalDateTime;
import quickfix.*;
import quickfix.field.*;
import quickfix.fix44.NewOrderSingle;

public class FixClientApplication extends MessageCracker implements Application {
    private SessionID sessionID;
    private final GlobalService globalService;

    FixClientApplication(GlobalService globalService) {
        this.globalService = globalService;
    }

    @Override
    public void onCreate(SessionID sessionId) {
        this.sessionID = sessionId;
    }

    @Override
    public void onLogon(SessionID sessionId) {
        System.out.println("Logged on with session: " + sessionId);
        this.globalService.setSessionID(sessionID);
    }

    @Override
    public void onLogout(SessionID sessionId) {
        System.out.println("Logged out with session: " + sessionId);
    }

    @Override
    public void toAdmin(Message message, SessionID sessionId) {
    }

    @Override
    public void fromAdmin(Message message, SessionID sessionId){
    }

    @Override
    public void toApp(Message message, SessionID sessionId) {
    }

    @Override
    public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectTagValue, UnsupportedMessageType {
        crack(message, sessionId);
    }

    public void sendOrder() throws SessionNotFound {
        NewOrderSingle order = new NewOrderSingle(
                new ClOrdID("123456"), // 客户端订单ID
                new Side(Side.BUY), // 买卖方向
                new TransactTime(LocalDateTime.now()), // 交易时间
                new OrdType(OrdType.MARKET) // 订单类型
        );
        Session.sendToTarget(order, sessionID);
    }

}
