package org.example.b1b.service;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;
import quickfix.*;
import java.io.IOException;
import java.io.InputStream;

@Component
public class FixInitiator {
    private static final String CONFIG_FILE_PATH = "quickfixj.cfg";
    private final SocketInitiator initiator;

    FixInitiator(GlobalService globalService) {
        try (InputStream inputStream = new ClassPathResource(CONFIG_FILE_PATH).getInputStream()) {
            SessionSettings settings = new SessionSettings(inputStream);
            FixClientApplication application = new FixClientApplication(globalService);
            MessageStoreFactory storeFactory = new FileStoreFactory(settings);
            LogFactory logFactory = new ScreenLogFactory(false, false, false);
            MessageFactory messageFactory = new quickfix.fix44.MessageFactory();
            this.initiator = new SocketInitiator(application, storeFactory, settings, logFactory, messageFactory);
        } catch (ConfigError | IOException e) {
            System.err.println("Fix Initiator Error: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @PostConstruct
    public void initializeFixInitiator() throws ConfigError {
        initiator.start();
    }

    @PreDestroy
    public void cleanupFixInitiator() {
        if (initiator != null) {
            initiator.stop();
        }
    }
}
