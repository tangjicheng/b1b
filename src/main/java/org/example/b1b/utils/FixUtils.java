package org.example.b1b.utils;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Optional;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import quickfix.Message;
import quickfix.InvalidMessage;

public final class FixUtils {
    private FixUtils() {
        throw new AssertionError("No FixUtils instances");
    }

    private static final Pattern FIX_PATTERN = Pattern.compile("8=FIX.*?\\u000110=\\d+\\u0001");

    public static String extractFIXPart(String input) {
        Matcher matcher = FIX_PATTERN.matcher(input);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "";
        }
    }

    public static Optional<Message> parseOneLine(String line) {
        String rawFIXMessage = extractFIXPart(line);
        if (rawFIXMessage.isEmpty()) {
            return Optional.empty();
        }

        try {
            Message message = new Message(rawFIXMessage);
            return Optional.of(message);
        } catch (InvalidMessage ex) {
            return Optional.empty();
        }
    }

    public static List<Message> parseOrdersFromFile(String filePath) {
        List<Message> ordersList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;

            while ((line = reader.readLine()) != null) {
                if (line.contains("35=D")) {
                    Optional<Message> newOrderOpt = parseOneLine(line);
                    newOrderOpt.ifPresent(ordersList::add);
                } else if (line.contains("35=F")) {
                    Optional<Message> cancelOrderOpt = parseOneLine(line);
                    cancelOrderOpt.ifPresent(ordersList::add);
                }
            }
        } catch (IOException e) {
            System.err.println("Unable to open file: " + filePath);
            System.err.println("Error: " + e.getMessage());
        }

        return ordersList;
    }

}
