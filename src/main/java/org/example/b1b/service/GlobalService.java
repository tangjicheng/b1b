package org.example.b1b.service;

//import lombok.Getter;
//import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import quickfix.SessionID;

@Component
public class GlobalService {
    private SessionID sessionID;

    @Value("${replay.file.path}")
    private String ReplayFilePath;

    public String getReplayFilePath() {
        return ReplayFilePath;
    }

    public SessionID getSessionID() {
        return sessionID;
    }

    public void setSessionID(SessionID sessionID) {
        this.sessionID = sessionID;
    }
}