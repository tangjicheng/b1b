package org.example.b1b.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {
    @GetMapping("/api/healthy")
    public String checkHealth() {
        return "OK\n";
    }
}
