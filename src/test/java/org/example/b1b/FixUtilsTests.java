package org.example.b1b;

import java.util.Optional;

import org.example.b1b.utils.FixUtils;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.boot.test.context.SpringBootTest;

import quickfix.Message;

@SpringBootTest
public class FixUtilsTests {

    @Test
    public void testExtractFIXPart_ValidInput_ReturnsCorrectPart() {
        // 测试有效的FIX字符串输入
        String input = "D0101 06/23/2024 12:08:12.246233 | 10.14.16.110 |  |         36 | recv 8=FIX.4.4\u00019=185\u000135=F\u000134=3\u000149=FT_SIT_D_2\u000152=20240623-03:08:12.246\u000156=s_t3\u00011=FT_SIT_ACCOUNT_2\u000111=9002022171911209200000002863\u000141=900202217191120920000000180\u000154=2\u000155=160030019\u000160=20240623-03:08:12.246039\u000110=202\u0001\n";
        String expected = "8=FIX.4.4\u00019=185\u000135=F\u000134=3\u000149=FT_SIT_D_2\u000152=20240623-03:08:12.246\u000156=s_t3\u00011=FT_SIT_ACCOUNT_2\u000111=9002022171911209200000002863\u000141=900202217191120920000000180\u000154=2\u000155=160030019\u000160=20240623-03:08:12.246039\u000110=202\u0001";
        assertEquals(expected, FixUtils.extractFIXPart(input), "Should extract the correct FIX part");
    }

    @Test
    public void testExtractFIXPart_InvalidInput_ReturnsEmptyString() {
        // 测试无效的FIX字符串输入
        String input = "Hello, world!";
        assertEquals("", FixUtils.extractFIXPart(input), "Should return an empty string for invalid input");
    }

    @Test
    public void testParseOneLine_ValidFIXMessage_ReturnsMessage() {
        // 测试能够正确解析的FIX消息
        String line = "D0101 06/23/2024 12:08:12.246233 | 10.14.16.110 |  |         36 | recv 8=FIX.4.4\u00019=185\u000135=F\u000134=3\u000149=FT_SIT_D_2\u000152=20240623-03:08:12.246\u000156=s_t3\u00011=FT_SIT_ACCOUNT_2\u000111=9002022171911209200000002863\u000141=900202217191120920000000180\u000154=2\u000155=160030019\u000160=20240623-03:08:12.246039\u000110=202\u0001\n";
        Optional<Message> result = FixUtils.parseOneLine(line);
        assertTrue(result.isPresent(), "Should return a present Optional for a valid FIX message");
    }

    @Test
    public void testParseOneLine_InvalidFIXMessage_ReturnsEmpty() {
        // 测试无法解析的FIX消息
        String line = "Not a FIX message";
        Optional<Message> result = FixUtils.parseOneLine(line);
        assertTrue(result.isEmpty(), "Should return an empty Optional for an invalid FIX message");
    }
}
